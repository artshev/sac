#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int Stringlen(char *String)
{
    int Count = 0;
    if(String)
    {
        // NOTE: lets not loop forever if string is not terminated
        while(String[Count] != 0 && Count < 100)
        {
            Count ++;
        }
    }
    
    return Count;
}

void PrintWords(char *array[], int Len)
{
    for(int i = 0; i < Len; i++)
    {
        printf("#%d %s, len %d\n", i, array[i], Stringlen(array[i]));
    }
    printf("\n");
}

int SortLex(const void * a, const void * b)
{
    const char *A = *(const char**)a;
    const char *B = *(const char**)b;
    
    return strcmp(A, B);
}

int SortLen(const void * a, const void * b) 
{
    char *A = *(char**)a;
    char *B = *(char**)b;
    
    if(Stringlen(A) < Stringlen(B))
        return 1;
    else if(Stringlen(A) > Stringlen(B))
        return -1;
    else 
        return 0;
}

struct data
{
    char Char;
    int Start;
    int End;
};

void main(int arg, char *argv[])
{
    
    char *words[] = {"jord", "robin", "xamarin", "zorro", "bilbo", 
                     "artiem", "fungus", "fungi", "fun", "funck", 
                     "funny", "fu", "apple", "app", "mango"};    
    int WordsLen = sizeof(words)/sizeof(char *);
     
    data chars_info[26] = {0};
    int CharsInfoLen = sizeof(chars_info)/sizeof(data);
    
    // #1 sort the words[] lexicographically
    qsort(words, WordsLen, sizeof(char *), SortLex);
    
#if 1
    printf("\nwords in list:\n--------------\n");
    PrintWords(words, WordsLen);
#endif
    
    // #2 gather indexing info in words[]
    bool FirstFind = false;
    for(int i = 0; i < WordsLen-1; i++)
    {
        char *Current = words[i]; 
        int CurrentInfoIndex = (int)Current[0] - 97;
        
        char *Next = words[i+1];               
        int NextInfoIndex = (int)Next[0] - 97;
        
        if(Current[0] != Next[0])
        {  
            // NOTE: Handle the first case
            if(!FirstFind)
            {
                chars_info[CurrentInfoIndex].Char = Current[0];               
                FirstFind = true;
            }
                       
            chars_info[CurrentInfoIndex].End = i;
            chars_info[NextInfoIndex].Char = Next[0];               
            chars_info[NextInfoIndex].Start = i+1;
            chars_info[NextInfoIndex].End = i+1;
        }
    }
        
#if 0
    // print indexing info
    for(int i = 0; i < CharsInfoLen; i++)
    {
        if(chars_info[i].Char)
        {
            printf("%c %d-%d\n", chars_info[i].Char, chars_info[i].Start, chars_info[i].End);
        }        
    }
    printf("\n");
#endif
    
    // #3 do the search leveraging on #1 and #2
    char *InputString = argv[1];
    int InputStringLen = Stringlen(InputString);
    int Start = 0;
    int End = 0;
    bool FirstMatch = false;
    
    if(InputString)
    {
        printf("looking for \"%s\"\n------------------\n", InputString);
  
        for(int i = 0; i < CharsInfoLen; i++)
        { 
            if(chars_info[i].Char && 
               chars_info[i].Char == InputString[0])
            {
                Start = chars_info[i].Start;
                End = chars_info[i].End;
                FirstMatch = true;
                break;
            }
        }
        
        if(FirstMatch)
        {
            bool SecondMatch = false;
            for(int i = Start; i <= End; i++)
            {
                char *TargetString = words[i];
                int HitCount = 0;
                
                while(HitCount < InputStringLen && 
                      TargetString[HitCount] == InputString[HitCount])
                {
                    HitCount++;   
                }
                
                if(HitCount == InputStringLen)
                {
                    printf("found %s\n", words[i]);
                    SecondMatch = true;
                }
            }
            
            if(!SecondMatch)
            {
                printf("nothing found\n");
            }
        }
        else
        {
            printf("nothing found\n");
        }
    }
    else
    {
        printf("no input\n");
    }    
}

